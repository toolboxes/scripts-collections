import platform
import sys

def get_system_info():
    system_info = {
        "Python Version": platform.python_version(),
        "Python Compiler": platform.python_compiler(),
        "Python Implementation": platform.python_implementation(),
        "Python Build": platform.python_build(),
        "System": platform.system(),
        "Node Name": platform.node(),
        "Release": platform.release(),
        "Version": platform.version(),
        "Machine": platform.machine(),
        "Processor": platform.processor(),
        "System Architecture": platform.architecture(),
        "Executable": sys.executable,
        "Filesystem Encoding": sys.getfilesystemencoding()
    }
    return system_info

if __name__ == "__main__":
    system_info = get_system_info()

    print("# System Information\n")
    for key, value in system_info.items():
        print(f"* **{key}:** {value}")
