# Scripts Collection

Put your public and installation-free use scripts here

## Development

### Issues

#### Context

To get context for issue, use this script


```
curl -s https://git.ias.u-psud.fr/toolboxes/scripts-collections/-/raw/main/Development/Issues/issue_infos.py | python
```

It provides infos to copy and paste in issues

```
# System Information

* **Python Version:** 3.11.7
* **Python Compiler:** GCC 9.3.0
* **Python Implementation:** CPython
* **Python Build:** ('main', 'Jan 12 2024 10:27:32')
* **System:** Linux
* **Node Name:** DESKTOP-91MSU61
* **Release:** 4.4.0-19041-Microsoft
* **Version:** #3636-Microsoft Thu Oct 19 17:41:00 PST 2023
* **Machine:** x86_64
* **Processor:** x86_64
* **System Architecture:** ('64bit', 'ELF')
* **Executable:** /home/someone/.pyenv/versions/3.11.7/bin/python
* **Filesystem Encoding:** utf-8
```

